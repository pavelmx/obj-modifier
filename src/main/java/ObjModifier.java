import java.nio.FloatBuffer;
import java.util.Arrays;

public class ObjModifier {
    public static void centerVertices(FloatBuffer buf) {
        // We often get 3D models which are not positioned at the origin of the coordinate system. Placing such models
        // on a 3D map is harder for users. One solution is to reposition such a model so that its center is at the
        // origin of the coordinate system. Your task is to implement the centering.
        //
        // FloatBuffer stores the vertices of a mesh in a continuous array of floats (see below)
        // [x0, y0, z0, x1, y1, z1, ..., xn, yn, zn]
        // This kind of layout is common in low-level 3D graphics APIs.
        // TODO: Implement your solution here

        //write coordinates from FloatBuffer to two dimensional array
        //[x0, y0, z0]
        //[x1, y1, z1]
        //[x2, y2, z2]
        float[][] vertices = new float[buf.limit() / 3][3];
        for (int i = 0; i < vertices.length; i++) {
            vertices[i] = new float[]{buf.get(), buf.get(), buf.get()};
        }

        //get center coordinates of sphere around the model
        float[] center = getCenter(vertices);

        //recalculate coordinates relative to the center
        float[][] newVertices = moveToCenter(vertices, center[0], center[1], center[2]);

        //two dimensional array -> one dimensional array
        float[] arrayVertices = flatten(newVertices);

        buf.clear();
        buf.put(arrayVertices);

    }

    private static float[] getCenter(float[][] vertices) {
        float[][] minMaxVertices = maxMinValue(vertices);
        float[] max =  minMaxVertices[0];
        float[] min =  minMaxVertices[1];
        //calculate center coordinates
        float cx = (min[0] + max[0]) / 2.0f;
        float cy = (min[1] + max[1]) / 2.0f;
        float cz = (min[2] + max[2]) / 2.0f;

        return new float[]{cx, cy, cz};
    }

    private static float[][] moveToCenter(float[][] vertices, float cx, float cy, float cz) {
        float[][] newVertices = new float[vertices.length][3];
        //calculate new coordinates
        for (int i = 0; i < vertices.length; i++) {
            float x = vertices[i][0] - cx;
            float y = vertices[i][1] - cy;
            float z = vertices[i][2] - cz;
            newVertices[i] = new float[]{x, y, z};
        }
        return newVertices;
    }

    private static float[][] maxMinValue(float[][] vertices) {
        float[] vertexMax = new float[]{Integer.MIN_VALUE,Integer.MIN_VALUE,Integer.MIN_VALUE};
        float[] vertexMin = new float[]{Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE};
        //find max and min vertices
        for (float[] vertex : vertices) {
            for (int column = 0; column < vertices[0].length; column++) {
                if (vertex[column] > vertexMax[column]) {
                    vertexMax[column] = vertex[column];
                }
                if (vertex[column] < vertexMin[column]) {
                    vertexMin[column] = vertex[column];
                }
            }
        }
        return new float[][]{vertexMax, vertexMin};
    }

    public static float[] flatten(float[][] source) {
        int size = Arrays.stream(source).mapToInt(floats -> floats.length).sum();

        float[] dest = Arrays.copyOf(source[0], size);
        int destPos = source[0].length;
        for (int i=1; i < source.length; i++) {
            System.arraycopy(source[i], 0, dest, destPos, source[i].length);
            destPos += source[i].length;
        }
        return dest;
    }

}
